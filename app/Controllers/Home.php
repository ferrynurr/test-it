<?php

namespace App\Controllers;


class Home extends BaseController
{
	protected  $controller = 'home';

	public function __construct()
	{
		$db = db_connect();
		$this->model      = model('HomeModel', true, $db);
		$this->dtbl      = model('DatatablesModel', true, $db);
		$this->cat       = model('CategoryModel', true, $db);
	}

	public function index()
	{
		$data = [
			'page' => $this->controller,
			'category' => $this->cat->findAll(),
		];

		return view('home', $data);
	}

	public function insert()
	{
		$data = [
			'name' => $this->request->getPost('name'),
			'category_id' =>  $this->request->getPost('category_id'),
			'description' =>  $this->request->getPost('description'),
		];

		$this->model->insert($data);
		return redirect()->to(base_url($this->controller));
	}

	public function update()
	{
		$data = [
			'name' => $this->request->getPost('name'),
			'category_id' =>  $this->request->getPost('category_id'),
			'description' =>  $this->request->getPost('description'),
		];

		$this->model->update($this->request->getPost('prod_id'), $data);
		return redirect()->to(base_url($this->controller));
	}

	public function getDatatables()
	{
		/** NEED csrf except this function in $globals "/config/filter"  */


		$config = [
			'table' => 'product',
			'column_order' => [null, 'product.name', 'category.name', 'product.description', null],
			'column_search' => ['product.name', 'category.name', 'product.description'],
			'order' => ['category.name' => 'asc'],
			'select' => '(category.name) category, product.*',
			'join' => [
				'category' => 'category.id = product.category_id',
			],
			'where' => null, //'(auth_groups.description = "Admin Event" OR auth_groups.description = "Guest") AND (auth_permissions.description = "Access to Android")',
			'group' => null,
		];

		$lists = $this->dtbl->get_datatables($config);
		$data = [];
		$no = $this->request->getPost("start");
		foreach ($lists as $list) {
			$no++;
			$row = [];

			$edit = '<button  type="button" class="btn btn-primary btn-sm" onclick="edit(' . $list->id . ')"> Edit</button>';
			$del = '<a class="btn btn-danger btn-sm" href="' . base_url($this->controller . '/delete/' . $list->id) . '">Delete</a>';


			$row[] = $no;
			$row[] = $list->category;
			$row[] = $list->name;
			$row[] = substr($list->description, 0, 70) . '...';
			$row[] =  $edit . '&nbsp;' . $del;
			$data[] = $row;
		}
		$output = [
			"draw" => $this->request->getPost('draw'),
			"recordsTotal" => $this->dtbl->count_all($config),
			"recordsFiltered" => $this->dtbl->count_filtered($config),
			"data" => $data
		];
		return $this->response->setJSON($output);
	}

	public function delete($id)
	{

		$this->model->delete($id);

		return redirect()->to(base_url($this->controller));
	}

	public function getbyId()
	{

		$response =  $this->model->where('id', $this->request->getVar('ID'))->first();

		return $this->response->setJSON($response);
	}
}
