<?php

namespace App\Controllers;


class Category extends BaseController
{
    protected  $controller = 'category';

    public function __construct()
    {
        $db = db_connect();
        $this->model      = model('CategoryModel', true, $db);
        $this->dtbl      = model('DatatablesModel', true, $db);
    }

    public function index()
    {
        $data = [
            'page' => $this->controller,
        ];

        return view('category', $data);
    }

    public function insert()
    {
        $data = [
            'name' => $this->request->getPost('name'),
            'description' =>  $this->request->getPost('description'),
        ];

        $this->model->insert($data);
        return redirect()->to(base_url($this->controller));
    }

    public function update()
    {
        $data = [
            'name' => $this->request->getPost('name'),
            'description' =>  $this->request->getPost('description'),
        ];

        $this->model->update($this->request->getPost('category_id'), $data);
        return redirect()->to(base_url($this->controller));
    }

    public function getDatatables()
    {
        /** NEED csrf except this function in $globals "/config/filter"  */


        $config = [
            'table' => 'category',
            'column_order' => [null, 'name',  'description', null],
            'column_search' => ['name',  'description'],
            'order' => ['name' => 'asc'],
            'select' => '*',
            'join' => null,
            'where' => null, //'(auth_groups.description = "Admin Event" OR auth_groups.description = "Guest") AND (auth_permissions.description = "Access to Android")',
            'group' => null,
        ];

        $lists = $this->dtbl->get_datatables($config);
        $data = [];
        $no = $this->request->getPost("start");
        foreach ($lists as $list) {
            $no++;
            $row = [];

            $edit = '<button  type="button" class="btn btn-primary btn-sm" onclick="edit(' . $list->id . ')"> Edit</button>';
            $del = '<a class="btn btn-danger btn-sm" href="' . base_url($this->controller . '/delete/' . $list->id) . '">Delete</a>';


            $row[] = $no;
            $row[] = $list->name;
            $row[] = substr($list->description, 0, 70) . '...';
            $row[] =  $edit . '&nbsp;' . $del;
            $data[] = $row;
        }
        $output = [
            "draw" => $this->request->getPost('draw'),
            "recordsTotal" => $this->dtbl->count_all($config),
            "recordsFiltered" => $this->dtbl->count_filtered($config),
            "data" => $data
        ];
        return $this->response->setJSON($output);
    }

    public function delete($id)
    {

        $this->model->delete($id);

        return redirect()->to(base_url($this->controller));
    }

    public function getbyId()
    {

        $response =  $this->model->where('id', $this->request->getVar('ID'))->first();

        return $this->response->setJSON($response);
    }
}
