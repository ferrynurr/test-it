<?= $this->extend('layouts/template'); ?>

<?= $this->section('content'); ?>
<div class="text-right mb-3">
    <button type="button" onclick="tambah()" class="btn btn-success">Tambah</button>
</div>

<div class="table-responsive">
    <table class="table table-striped" style="width:100%" id="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Name</th>

                <th scope="col">Desc</th>
                <th scope="col">#</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= $page ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="content_modal"></div>
            </div>
        </div>
    </div>
</div>

<script>
    let tables;

    $(function() {

        tables = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: "<?= base_url($page . '/getDatatables') ?>",
                type: "post",
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false,
                    "width": '5%',
                },
                {
                    "targets": [3],
                    "orderable": false,
                    "width": '10%',
                },
            ],

        });

        $('#categori').addClass('active');
    });

    tambah = () => {
        $('#exampleModal').modal('show');
        let html = `
			<form action="<?= base_url($page . '/insert') ?>" method="post">
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="name" id="name" required>
	
				</div>
				<div class="form-group">
					<label>Desc</label>
					<input type="text" class="form-control" id="description" name="description">
				</div>
				
				<button type="submit" class="btn btn-primary">Submit</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</form>
		`;

        $('#content_modal').html(html);
    }

    edit = (id) => {

        let url = '<?= base_url($page) ?>/getbyId';
        $.get(url, {
                ID: id,
            })
            .done(function(res) {
                console.log(res);
                $('#exampleModal').modal('show');
                let html = `
						<form action="<?= base_url($page . '/update') ?>" method="post">
							<div class="form-group">
							<input type="hidden" name="category_id" id="category_id" >
								<label>Name</label>
								<input type="text" class="form-control" name="name" id="name" value"" required>
				
							</div>
							<div class="form-group">
								<label>Desc</label>
								<input type="text" class="form-control" id="description" name="description">
							</div>
							
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</form>
					`;

                $('#content_modal').html(html);

                $('#category_id').val(res.id);
                $('#name').val(res.name);
                $('#description').val(res.description);
            });


    }
</script>
<?= $this->endSection(); ?>