<!DOCTYPE html>
<html lang="en">

<head>
    <title>TEST IT </title>

    <meta charset="utf-8">

    <meta name="description" content="BEM UNAIR DEKAT Profile and marketplace">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"> -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="EWI-STUDIO">


    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/node_modules/bootstrap/dist/css/bootstrap.min.css') ?>">
    <script type="text/javascript" src="<?= base_url('assets/node_modules/jquery/dist/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/node_modules/popper.js/dist/popper.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

    <!-- Fontawesome -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/font-awesome-4.7.0/css/font-awesome.min.css') ?>"> -->

    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.js"></script>
    <!-- datepicker -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css') ?>">
    <script type="text/javascript" src="<?= base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"></script> -->


    <style {csp-style-nonce}>

    </style>

</head>

<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="#">ferryApp</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item" id="home">
                        <a class="nav-link" href="<?= base_url('') ?>">Barang </a>
                    </li>
                    <li class="nav-item" id="categori">
                        <a class="nav-link" href="<?= base_url('category') ?>">Kategori</a>
                    </li>
                </ul>
            </div>
        </nav>

        <br>
        <br>