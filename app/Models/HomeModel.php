<?php

namespace App\Models;

use CodeIgniter\Model;

class HomeModel extends Model
{
    protected $table      = 'product';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'category_id', 'description'];

    private function query()
    {
        return $this->db->table('auth_groups_users')
            ->select('users.*, auth_groups.name')
            ->join('users', 'users.id = auth_groups_users.user_id', 'left')
            ->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id', 'left');
    }


    public function getUserGroup($id, $data = null)
    {
        $query = $this->query();
        if ($data) {
            $query->whereIn($id, $data);
        }

        $result = $query->get();

        return $result;
    }
}
