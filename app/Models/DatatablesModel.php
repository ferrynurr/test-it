<?php

namespace App\Models;

use CodeIgniter\Model;

class DatatablesModel extends Model
{
    protected $table;
    protected $column_order; // array
    protected $column_search; //array
    protected $order; //array

    protected $dt;

    protected $input;

    function __construct()
    {
        parent::__construct();
        $this->input = \Config\Services::request();
    }

    private function query($config)
    {
        // global
        $this->table = $config['table'];
        $this->column_order =  $config['column_order'];
        $this->column_search = $config['column_search'];
        $this->order =  $config['order'];

        // private
        $select =  $config['select'];
        $join = $config['join'];
        $where = $config['where'];
        $group = $config['group'];

        $this->dt = $this->db->table($this->table);
        $this->dt->select($select);

        if ($join) {
            foreach ($join as $key => $value) {
                $this->dt->join($key, $value, 'left');
            }
        }

        if ($where)
            $this->dt->Where($where);

        if ($group)
            $this->dt->groupBy($group);
    }

    private function _get_datatables_query($config)
    {
        $this->query($config);

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($this->input->getPost('search')['value']) {
                if ($i === 0) {
                    $this->dt->groupStart();
                    $this->dt->like($item, $this->input->getPost('search')['value']);
                } else {
                    $this->dt->orLike($item, $this->input->getPost('search')['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->dt->groupEnd();
            }
            $i++;
        }

        if ($this->input->getPost('order')) {
            $this->dt->orderBy($this->column_order[$this->input->getPost('order')['0']['column']], $this->input->getPost('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->dt->orderBy(key($order), $order[key($order)]);
        }
    }

    function get_datatables($config)
    {
        $this->_get_datatables_query($config);
        if ($this->input->getPost('length') != -1)
            $this->dt->limit($this->input->getPost('length'), $this->input->getPost('start'));
        $query = $this->dt->get();
        return $query->getResult();
    }

    function count_filtered($config)
    {
        $this->_get_datatables_query($config);
        return $this->dt->countAllResults();
    }

    public function count_all($config)
    {
        $this->query($config);
        $query = $this->dt->get()->getResult();

        return count($query);
    }
}
